<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class homeController extends Controller
{
    public function data(){
        return view('Admin.data');
    }

    public function datatable(){
        return view('Admin.datatable');
    }
}
