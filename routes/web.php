<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Admin.login');
});

Route::get('/homeadmin',  function(){
    return view('Admin.home');
});
Route::post('/homeadmin' , 'AuthController@loginadmin');

Route::get('/data' , 'homeController@data');

Route::get('/data-table', 'homeController@datatable');
